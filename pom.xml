<?xml version="1.0" encoding="UTF-8"?>
<!--#======================================================================= -->
<!--# Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany). -->
<!--# All rights reserved. This program and the accompanying materials -->
<!--# are made available under the terms of the Eclipse Public License 2.0  -->
<!--# which accompanies this distribution, and is available at -->
<!--# https://www.eclipse.org/legal/epl-2.0/    -->
<!--#                                           -->
<!--# SPDX-License-Identifier: EPL-2.0          -->
<!--# -->
<!--# Contributors: -->
<!--# Christophe Loetz  (Loetz GmbH&Co.KG) - Initial implementation API and implementation -->
<!--#======================================================================= -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.eclipse.osbp.releng.maven</groupId>
		<artifactId>org.eclipse.osbp.releng.maven.parent.tycho</artifactId>
		<version>0.9.0-SNAPSHOT</version>
	</parent>

	<groupId>org.eclipse.osbp.technologystack</groupId>
	<artifactId>org.eclipse.osbp.technologystack</artifactId>
	<packaging>pom</packaging>

	<url>${osbp.site.repository.url}</url>
	<scm>
		<url>${osbp.scm.url}</url>
		<connection>${osbp.scm.connection}</connection>
		<developerConnection>${osbp.scm.connection.dev}</developerConnection>
		<tag>HEAD</tag>
	</scm>
	<distributionManagement>
		<site>
			<id>gh-pages</id>
			<name>OSBP GitHub Pages</name>
			<url>${distribution.site.url}</url>
		</site>
	</distributionManagement>

	<repositories>
	    <repository>
            <id>OSBP local dependencies</id>
            <url>file://${osbp.developer.home.repositories}${osbp.dependencies.repository.label}/neon/${osbp.build.branch}</url>
            <layout>p2</layout>
        </repository>
	</repositories>

	<properties>
		<license.copyrightOwners>Lunifera GmbH, Loetz GmbH&amp;Co.KG</license.copyrightOwners>
		<osbp.gitrepo.name>${project.groupId}</osbp.gitrepo.name>
		<osbp.skip.sonar>true</osbp.skip.sonar>
	</properties>

	<modules>
		<module>org.eclipse.osbp.technologystack.feature</module>
		<module>org.eclipse.osbp.technologystack.target.feature</module>
		<module>org.eclipse.osbp.technologystack.postgresql.feature</module>
		<module>org.eclipse.osbp.technologystack.p2</module>
	</modules>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>target-platform-configuration</artifactId>
					<version>${tycho-version}</version>
					<configuration>
						<resolver>p2</resolver>
						<pomDependencies>consider</pomDependencies>
						<environments>
							<environment>
								<os>win32</os>
								<ws>win32</ws>
								<arch>x86_64</arch>
							</environment>
							<environment>
								<os>linux</os>
								<ws>gtk</ws>
								<arch>x86</arch>
							</environment>
							<environment>
								<os>linux</os>
								<ws>gtk</ws>
								<arch>x86_64</arch>
							</environment>
							<environment>
								<os>macosx</os>
								<ws>cocoa</ws>
								<arch>x86_64</arch>
							</environment>
						</environments>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
		<plugins>
			<plugin>
				<artifactId>maven-clean-plugin</artifactId>
				<configuration>
					<filesets>
						<fileset>
							<directory>xtend-gen</directory>
							<includes>
								<include>**</include>
							</includes>
						</fileset>
					</filesets>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.eclipse.xtend</groupId>
				<artifactId>xtend-maven-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>compile</goal>
							<goal>testCompile</goal>
						</goals>
						<configuration>
							<outputDirectory>xtend-gen</outputDirectory>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.codehaus.sonar-plugins</groupId>
				<artifactId>maven-report</artifactId>
				<configuration>
					<skip>true</skip>
				</configuration>
			</plugin>
		</plugins>
	</build>
	<reporting>
		<plugins>
			<plugin>
				<groupId>org.codehaus.sonar-plugins</groupId>
				<artifactId>maven-report</artifactId>
				<configuration>
					<skip>true</skip>
				</configuration>
			</plugin>
		</plugins>
	</reporting>
</project>
